﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Net;
using System.Text;
using System.Windows.Forms;

namespace FaceBookSample
{
    public partial class Form1 : Form
    {
        public static String pValue(String Data, String Begin, String End)
        {
            return pValue(Data, Begin, End, 1);
        }
        public static String pValue(String Data, String Begin, String End, int Pointer = 1)
        {
            try
            {
                int[] Counter = { 0, 0, 0 };

                for (int i = 0; i < Data.Length; i++)
                {
                    if (Begin == Data.Substring(i, Begin.Length))
                    {
                        Counter[2] = i + Begin.Length;
                        for (int j = Counter[2]; j < Data.Length; j++)
                        {
                            if (End == Data.Substring(j, End.Length))
                            {
                                Counter[0]++;
                                Counter[1] = j;
                                if (Counter[0] != Pointer) { break; }
                                return Data.Substring(Counter[2], Counter[1] - Counter[2]);
                            }
                        }
                    }
                }
                return "";
            }
            catch
            // catch (ArgumentException e)
            {
                // e.ToString();
                return "";
            }
        }


        public static String _gHTML(String URL, String REFERER, bool hProxy = false)
        {
            WebProxy wProxy = new WebProxy();
            HttpWebRequest REQ = (HttpWebRequest)WebRequest.Create(URL);
            if (hProxy)
            {
                wProxy = new WebProxy("http://127.0.0.1:8080");
                wProxy.BypassProxyOnLocal = true;
            }
            REQ.Referer = REFERER;
            REQ.AllowAutoRedirect = true;
            REQ.ServicePoint.Expect100Continue = false;
            REQ.Timeout = 10000;
            REQ.CookieContainer = SID;
            REQ.Method = "GET";
            REQ.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:21.0) Gecko/20100101 Firefox/21.0";
            if (hProxy) { REQ.Proxy = wProxy; }
            HttpWebResponse RES = (HttpWebResponse)REQ.GetResponse();
            StreamReader SR = new StreamReader(RES.GetResponseStream());
            String rHTML = SR.ReadToEnd();
            SR.Close();
            RES.Close();
            return rHTML;
        }
        public static CookieContainer SID = new CookieContainer();
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click_1(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            try
            {


                string googooli = _gHTML("https://www.facebook.com/login.php?login_attempt=1", "https://www.facebook.com/", true);
                string lsd = pValue(googooli, "type=\"hidden\" name=\"lsd\" value=\"", "\" autocomplete=\"off\" /><div class=\"hidden_elem\"");
                string dp = pValue(googooli, "type=\"hidden\" autocomplete=\"off\" id=\"default_persistent\" name=\"default_persistent\" value=\"", "\" /><div id=\"buttons\" class=\"form_row clearfix\"><label class=\"login_form_label\">");
                string timezone = 420.ToString();
                string lgnrnd = pValue(googooli, "<input type=\"hidden\" name=\"lgnrnd\" value=\"", "\" />");
                string lgnjs = pValue(googooli, "<input type=\"hidden\" id=\"lgnjs\" name=\"lgnjs\" value=\"", "\" />");
                string locale = pValue(googooli, "<a href=\"/r.php?next&amp;locale=", "&amp;display=page");
                string fb_dtsg=pValue(googooli,"id=\"u_0_1\"><input type=\"hidden\" name=\"fb_dtsg\" value=\"","\" autocomplete=\"off\" /><input type=\"hidden\" autocomplete");
                Console.WriteLine(googooli);

               
                // /*
                WebProxy wProxy = new WebProxy();
                HttpWebRequest REQ = (HttpWebRequest)WebRequest.Create("https://www.facebook.com/login.php?login_attempt=1");
                if (true)
                {
                    wProxy = new WebProxy("http://127.0.0.1:8080");
                    wProxy.BypassProxyOnLocal = true;
                }
                REQ.AllowAutoRedirect = true;
                REQ.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:21.0) Gecko/20100101 Firefox/21.0";
                REQ.KeepAlive = true;
                REQ.Method = "POST";
                REQ.Referer = "https://www.facebook.com/";
                REQ.ServicePoint.Expect100Continue = false;
                REQ.ContentType = "application/x-www-form-urlencoded";
                REQ.ProtocolVersion = HttpVersion.Version10;
                REQ.CookieContainer = SID;
                // if (USECOOKIE) { REQ.CookieContainer = SID; }

                byte[] Bytes = Encoding.ASCII.GetBytes("lsd="+lsd+"&email="+textBox1.Text.ToString()+"&pass="+textBox2.Text.ToString()+"&default_persistent="+dp+"&timezone="+timezone+"&lgnrnd="+lgnrnd+"&lgnjs="+lgnjs+"&locale="+locale+"");

                REQ.ContentLength = Bytes.Length;

                if (true) { REQ.Proxy = wProxy; }

                Stream STM = REQ.GetRequestStream();

                STM.Write(Bytes, 0, Bytes.Length);

                STM.Close();

                HttpWebResponse RES = (HttpWebResponse)REQ.GetResponse();

                RES.Close();

                String VALUE = RES.Headers.ToString();
                /*
                return String.Concat(_gHTML(TARGETURL, TARGETURL),
                                     Constants.UNIQUEPOINTER.B,
                                     VALUE,
                                     Constants.UNIQUEPOINTER.E);
                */
            }
            catch
            {
                
            }
        }


    }
}
